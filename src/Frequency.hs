-- 01

module Frequency (solutions) where

import Data.List (find)
import Data.Text.Lazy (Text)
import Data.Text.Lazy.Read (signed, decimal)
import qualified Data.Text.Lazy as T (lines)
import qualified Data.Text.Lazy.IO as T (getContents)

import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet

castToMaybe :: Either String (Integer, Text) -> Maybe Integer
castToMaybe (Right v) = Just $ fst v
castToMaybe         _ = Nothing

readInteger :: Text -> Maybe Integer
readInteger = castToMaybe . signed decimal

filterValues :: [Maybe Integer] -> [Integer]
filterValues = foldr appendValue []
  where
    appendValue (Just v) xs = v:xs
    appendValue        _ xs = xs

getValues :: IO [Integer]
getValues = filterValues . fmap readInteger . T.lines <$> T.getContents

part1 :: IO ()
part1 = fmap (show . sum) getValues >>= putStrLn

--

findRepetition' :: [Integer] -> Either Integer [Integer]
findRepetition' = foldr searcher (Right [])
  where
    searcher :: Integer -> Either Integer [Integer] -> Either Integer [Integer]
    searcher _ (Left x) = Left x
    searcher v (Right xs)
      | v `elem` xs = Left v
      | otherwise   = Right (v:xs)

memList :: [Integer] -> [Integer] -> [(Integer, [Integer])]
memList (x:xs) ys = (x, ys) : memList xs (x:ys)
memList     []  _ = []

findRepetition :: [Integer] -> Maybe Integer
findRepetition xs = fmap fst result
  where
    memoized = memList xs []
    predicate (val, hist) = val `elem` hist
    result = find predicate memoized

part2 :: IO ()
part2 = do
  values <- getValues
  let freqs = scanl (+) 0 $ cycle values
  print (findRepetition freqs)

--

fastFindRepetition' :: [Integer] -> HashSet Integer -> Maybe Integer
fastFindRepetition' (x:xs) set
  | x `HashSet.member` set = Just x
  | otherwise              = fastFindRepetition' xs (HashSet.insert x set)
fastFindRepetition'  []  _ = Nothing

fastFindRepetition :: [Integer] -> Maybe Integer
fastFindRepetition xs = fastFindRepetition' xs HashSet.empty

part2' :: IO ()
part2' = do
  values <- getValues
  let freqs = scanl (+) 0 $ cycle values
  print (fastFindRepetition freqs)

solutions :: [IO ()]
solutions = [part1, part2, part2']
