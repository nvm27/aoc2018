module Main where

import qualified Frequency
import qualified Inventory
import qualified Fabric
import qualified Guards

main :: IO ()
main = last Guards.solutions
