-- 02

module Inventory (solutions) where

import GHC.Int (Int64)
import Data.List (maximumBy, find)
import Data.Maybe (isJust)

import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.IO as T (getContents)

import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

getIDs :: IO [Text]
getIDs = T.lines <$> T.getContents

countLetters :: Text -> HashMap Char Int
countLetters = T.foldr counter HashMap.empty
  where
    counter :: Char -> HashMap Char Int -> HashMap Char Int
    counter c = HashMap.insertWith (+) c 1

countExact :: Int -> HashMap Char Int -> Int
countExact count = length . HashMap.filter (== count)

countExactOccurrences :: Int -> [HashMap Char Int] -> Int
countExactOccurrences count = length . filter (\m -> countExact count m > 0)

part1 :: IO ()
part1 = do
  ids <- getIDs
  let maps = countLetters <$> ids
  let doubles = countExactOccurrences 2 maps
  let triples = countExactOccurrences 3 maps
  print (doubles * triples)

--

skipLetter :: Int -> Text -> Text
skipLetter n text = begin `T.append` end
  where
    i = fromIntegral n
    begin = T.take i text
    end = T.drop (i+1) text

skipLetterInList :: Int -> [Text] -> [Text]
skipLetterInList n = fmap (skipLetter n)

findDuplicate' :: HashSet Text -> [Text] -> Maybe Text
findDuplicate' set (x:xs)
  | x `HashSet.member` set = Just x
  | otherwise              = findDuplicate' (x `HashSet.insert` set) xs
findDuplicate'   _     []  = Nothing

findDuplicate :: [Text] -> Maybe Text
findDuplicate = findDuplicate' HashSet.empty

findDuplicateWithSkipped :: Int -> [Text] -> Maybe Text
findDuplicateWithSkipped n = findDuplicate . skipLetterInList n

checkForDuplicate :: Int -> [Text] -> Bool
checkForDuplicate n = isJust . findDuplicateWithSkipped n

applicableIndices :: Text -> [Int]
applicableIndices text = takeWhile (< size) [0..]
  where
    size = fromIntegral $ T.length text

getLongestEntry :: [Text] -> Text
getLongestEntry = maximumBy comp
  where
    comp a b = compare (T.length a) (T.length b)

part2 :: IO ()
part2 = do
  ids <- getIDs
  let indices = applicableIndices $ getLongestEntry ids
  let idx = find (`checkForDuplicate` ids) indices
  print $ fmap (`findDuplicateWithSkipped` ids) idx

--

solutions :: [IO ()]
solutions = [part1, part2]
