-- 03

{-# LANGUAGE TupleSections #-}

module Fabric (solutions) where

import Data.Array

import Data.Attoparsec.Text

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T (getContents)

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

type Id = Int
type Point = (Int, Int)
type PointWithId = (Point, Id)
type Rectangle = (Int, Point, Point)

rectangleParser :: Parser Rectangle
rectangleParser = do
  char '#'
  id <- decimal
  skipSpace
  char '@'
  skipSpace
  x <- decimal
  char ','
  y <- decimal
  char ':'
  skipSpace
  sizeX <- decimal
  char 'x'
  sizeY <- decimal
  return (id, (x, y), (x+sizeX-1, y+sizeY-1))

inputParser :: Parser [Rectangle]
inputParser = many' $ rectangleParser <* endOfLine

parseInput :: Text -> [Rectangle]
parseInput = cast . parseOnly inputParser
  where
    cast :: Either String [Rectangle] -> [Rectangle]
    cast (Right xs) = xs
    cast          _ = []

rectanglePoints :: Rectangle -> [PointWithId]
rectanglePoints (id, begin, end) = addId <$> range (begin, end)
  where
    addId :: Point -> PointWithId
    addId (x, y) = ((x, y), id)

generateOverlayArray :: Int -> Int -> [Rectangle] -> Array Point [Id]
generateOverlayArray w h rects = accumArray (flip (:)) [] size points
  where
    size = ((0, 0), (w-1, h-1))
    points = concatMap rectanglePoints rects

fromInputToOverlayArray :: Text -> Array Point [Id]
fromInputToOverlayArray = generateOverlayArray 1000 1000 . parseInput

part1 :: IO ()
part1 = do
  input <- T.getContents
  print $ length . filter (\x -> length x > 1) . elems $ fromInputToOverlayArray input

--

overlaySizeAtPoint :: [Id] -> [(Id, Int)]
overlaySizeAtPoint xs = fmap (, length xs) xs

generateMaxOverlaySize :: Array Point [Id] -> HashMap Int Int
generateMaxOverlaySize = HashMap.fromListWith max . concatMap overlaySizeAtPoint . elems

part2 :: IO ()
part2 = do
  input <- T.getContents
  print $ HashMap.filter (== 1) $ generateMaxOverlaySize $ fromInputToOverlayArray input

solutions :: [IO ()]
solutions = [part1, part2]
