-- 04

module Guards (solutions) where

import Control.Applicative
import Data.Attoparsec.Text
import Data.Time
import Data.List (sortOn)
import Data.Ord (comparing)

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T (getContents)

data LogInfo = BeginsShift Int | WakesUp | FallsAsleep
  deriving Show

data LogEntry = LogEntry {
  entryTime :: LocalTime,
  entryInfo :: LogInfo
} deriving Show

type Log = [LogEntry]

timestampParser :: Parser LocalTime
timestampParser = do
  y  <- count 4 digit
  char '-'
  mm <- count 2 digit
  char '-'
  d <- count 2 digit
  char ' '
  h <- count 2 digit
  char ':'
  m <- count 2 digit
  return $ LocalTime {
    localDay = fromGregorian (read y) (read mm) (read d),
    localTimeOfDay = TimeOfDay (read h) (read m) 0
  }

shiftParser :: Parser LogInfo
shiftParser = do
  string "Guard #"
  id <- decimal
  string " begins shift"
  return $ BeginsShift id

infoParser :: Parser LogInfo
infoParser =
      shiftParser
  <|> (string "wakes up" >> return WakesUp)
  <|> (string "falls asleep" >> return FallsAsleep)

logEntryParser :: Parser LogEntry
logEntryParser = do
  char '['
  timestamp <- timestampParser
  char ']'
  char ' '
  info <- infoParser
  return $ LogEntry timestamp info

logParser :: Parser Log
logParser = many' $ logEntryParser <* endOfLine

prepareInput :: Text -> Log
prepareInput = sortOn entryTime . cast . parseOnly logParser
  where
    cast :: Either String [a] -> [a]
    cast (Right xs) = xs
    cast          _ = []

part1 :: IO ()
part1 = do
  input <- T.getContents
  print $ prepareInput input


solutions :: [IO ()]
solutions = [part1]
